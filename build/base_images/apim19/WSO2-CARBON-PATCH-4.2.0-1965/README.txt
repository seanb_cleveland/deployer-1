Patch ID         : WSO2-CARBON-PATCH-4.2.0-1965
Applies To       : am-1.9.0
Associated JIRA  : https://wso2.org/jira/browse/APIMANAGER-5349


DESCRIPTION
-----------

This patch fix the NumberFormatException thrown when the Content-Length header is more than the Integer.MAX value.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the patch1965 to  <CARBON_SERVER>/repository/components/patches/ directory.

(iii) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

