Patch ID         : WSO2-CARBON-PATCH-4.2.0-1421
Applies To       : API Manager-1.9.0
Associated JIRA  : https://wso2.org/jira/browse/APIMANAGER-3912


DESCRIPTION
-----------
This is a fix for the "java.lang.IllegalStateException: The cache status is not STARTED" exception which occurs when invoking an API after sometimes when JWT caching is enabled.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch1421 to  <CARBON_SERVER>/repository/components/patches/

(iv) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

